const fs = require("fs");
const path = require("path");
const MatchcsvFilePath = path.resolve(__dirname, "../data/matches.csv");
const deliveryCsvFilePath = path.resolve(__dirname, "../data/deliveries.csv")
const csv=require('csvtojson')
const ipl = require("./ipl");
const { match } = require("assert");
csv()
.fromFile(MatchcsvFilePath)
.then((matches)=>{


csv()
.fromFile(deliveryCsvFilePath)
.then((delivery)=>{
    // console.log(delivery);
    // console.log(matches);


    // console.log(matches);

    // Number of matches played per year for all the years in IPL.
    let matchesPerYear = ipl.getMatchesPerYear(matches);
    console.log(matchesPerYear);
    writeFile(
        path.resolve(__dirname, "../public/output/matchesPerYear.json"),
        JSON.stringify(matchesPerYear,null, 2)
        );

    // Number of matches won per team per year in IPL.
    let matchesWonPerYear = ipl.noOfMatchesWonPerYear(matches);
    console.log(matchesWonPerYear);
    writeFile(
        path.resolve(__dirname, "../public/output/matchesWonPerYear.json"),
        JSON.stringify(matchesWonPerYear,null,2)
    );

    // Extra runs conceded per team in the year 2016
    let extraRunsIn2016 = ipl.extraRuns2016(matches,delivery,2016);
    console.log(extraRunsIn2016);

    writeFile(
        path.resolve(__dirname, "../public/output/extraRunsIn2016.json"),
        JSON.stringify(extraRunsIn2016,null,2)
    );

    
    // Top 10 economical bowlers in the year 2015
    let topEconomialBowler = ipl.topEconomialBowler(matches,delivery,2015);
    
    writeFile(
        path.resolve(__dirname, "../public/output/topEconomicalBowler.json"),
        JSON.stringify(topEconomialBowler,null,2)
    );
    console.log(topEconomialBowler);

   // Extra Questions
 // 1.Find the number of times each team won the toss and also won the match
 let noOfTimesWonTossAndMatch = ipl.noOfTimesWonTossAndMatch(matches);

 writeFile(
     path.resolve(__dirname, "../public/output/noOfTimesWonToassAndMatch.json"),
     JSON.stringify(noOfTimesWonTossAndMatch,null,2)
 );

 console.log(noOfTimesWonTossAndMatch);



 //  2.Find a player who has won the highest number of Player of the Match awards for each season
 let highestPlayerOfMatch = ipl.highestPlayerOfMatch(matches);

 writeFile(
     path.resolve(__dirname, "../public/output/highestPlayerOfMatch.json"),
     JSON.stringify(highestPlayerOfMatch,null,2)
 );

 console.log(highestPlayerOfMatch);


  // 3.Find the strike rate of a batsman for each season
 let strikeRateOfBatsman = ipl.strikeRateOfBatsman('V Kohli', matches, delivery);

 writeFile(
     path.resolve(__dirname,"../public/output/strikeRateOfBatsman.json"),
     JSON.stringify(strikeRateOfBatsman,null,2)
 );




// 4.Find the highest number of times one player has been dismissed by another player
let highestNoOfTimesDismissedPlayer = ipl.highestNoOfTimesDismissedPlayer(delivery);


// 5.Find the bowler with the best economy in super overs
let bestEconomyInSuperOvers = ipl.bestEconomyInSuperOvers(delivery);






 



    


    
});
    
    
});

function writeFile(path, data) {
    fs.writeFile(path, data, function (err){
        if(err)
        {
            console.log(err);
        }
    });
}


 


