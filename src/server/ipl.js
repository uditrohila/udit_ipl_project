function getMatchesPerYear(matches){
    let matchesPerYear = {};
    for(let i =0; i < matches.length; i++){
        let season = matches[i]["season"];
        if(!matchesPerYear[season]){
            matchesPerYear[season] = 1;
        }
        else{
            matchesPerYear[season] += 1;
        }
    }

    return matchesPerYear;
}




function noOfMatchesWonPerYear(matches){
    let winTeamObject = {};
    for(let index = 0; index < matches.length; index++){
        let year = matches[index].season;
        let winner = matches[index].winner;
        let key = "TEAM" + "_" + winner + "_" + year;
        if(!winTeamObject[key]){
            winTeamObject[key] = 1;
        }
        else
        {
            winTeamObject[key] += 1;
        }
    }

    return winTeamObject;
}


// Extra runs conceded per team in the year 2016
function extraRuns2016(matches,deliveries,year){
    const extraRuns = {};
    const matchIdSeasonMap = {};
    for(let match of matches){
        let season = match.season;
        let id = match.id;
        matchIdSeasonMap[id] = season;
    }
     
    // console.log(matchIdSeasonMap);

    for(let delObj of deliveries){
        let matchId = delObj["match_id"];
        if(matchIdSeasonMap[matchId] == year){
            // let val = delObj[batting_team];
            // console.log(delObj.batting_team);
            let exruns = parseInt(delObj.extra_runs);
            if(!extraRuns[delObj.batting_team]){
                  extraRuns[delObj.batting_team] = exruns;
            }
            else
            {
                extraRuns[delObj.batting_team] += exruns;
            }

        }
    }

    // console.log(extraRuns);
    return extraRuns;
}



// Top 10 economical bowlers in the year 2015
function topEconomialBowler(matches,deliveries,year){
    const economyBowler = {};

    const matchIdSeasonMap = {};
    for(let match of matches){
        let season = match.season;
        let id = match.id;
        matchIdSeasonMap[id] = season;
    }


    for(let delObj of deliveries){
        let matchId = delObj["match_id"];
        if(matchIdSeasonMap[matchId] == year){
         
            if(economyBowler[delObj.bowler] !== undefined){
                economyBowler[delObj.bowler][0] += parseInt(delObj.total_runs);
                economyBowler[delObj.bowler][1] += parseInt(delObj.ball);
            }
            else
            {
                // for each bowler create property of bowler name and
                // in value push the totalruns and overs
                economyBowler[delObj.bowler] = [];
                economyBowler[delObj.bowler].push(parseInt(delObj.total_runs),(parseInt(delObj.ball)));
            }

        }
    }


    let Arr = [];
    for(let key in economyBowler){
        let runs = Math.floor(economyBowler[key][0]);
        let over = Math.floor(economyBowler[key][1] / 6);
        let value = runs/over;
        Arr.push([key,value]);
    }

    Arr.sort(function(a,b){
        return a[1] - b[1];
    });

    let outputArr = [];
    for(let i =0; i < 10; i++){
        // console.log(Arr[i]);
        outputArr.push(Arr[i]);
    }

    // convert the array into object
    let result = Object.fromEntries(outputArr);
    return result;

}

// #############################################################################################
// extra Questions

// 1.Find the number of times each team won the toss and also won the match
// teamname -> toss win --> match won

function noOfTimesWonTossAndMatch(matches){
    let result = {};
    matches.forEach(matchobj => {
        if(matchobj.toss_winner === matchobj.winner){
            if(result[matchobj.winner] !== undefined){
                   result[matchobj.winner] += 1;
            }
            else{
                result[matchobj.winner] = 1;
            }
        }
    });

    // console.log(result);
    return result;
}

// #################################################################################################

function highestPlayerOfMatch(matches){
    let obj = {};
    for(let prop of matches){
        let year = prop.season;
        let playerName = prop.player_of_match;

        if(!obj[year]){
            obj[year] = {};
            obj[year][playerName] = 1;
        }
        else
        {   // if year key exist then check for playername property inside year object
            if(obj[year][playerName] === undefined){
                obj[year][playerName] = 1;
            }
            else
            {
                obj[year][playerName] += 1;
            }
        }
    }

    // console.log(obj);
    let result = {};
    // traverse each obj val then get enteries of key
    for(let arr in obj){    // get each object convert into array ie. [[key , value], [key,value]]
        let arrObj = Object.entries(obj[arr]).sort((a,b) => b[1] - a[1]);
        // console.log(arrObj);
        let finalResult = [arrObj[0]]; 
        // console.log(finalResult);
        // converting map to object
        let objarr = Object.fromEntries(finalResult);
        // update the object with highest object
        result[arr] = objarr;  // here arr means key i.e year
    }

    // console.log(result);
    return result;

}

// ##############################################################################################

// Find the strike rate of a batsman for each season
// (runsscored / ballface)*100
// steps 1. traverse on delivery and find obj with match name
// step 2. traverse over the match if match name obj id == match id then return season
// step 3. check for the season if new object searson key exist or not
// step 4 if new obj key not exist create array and push totalruns and balls
// step 5 this will done for all season
// output : []

function strikeRateOfBatsman(givenBatsMan, matches, deliveries){
   
    let year;
    // let givenBatsManStrike = givenBatsMan;
     let givenBatsManStrike = {};
    deliveries.map(delObj => {
        if(delObj.batsman == givenBatsMan){
            year = matches.find(matchObj => {
                if(matchObj.id == delObj.match_id){
                    return matchObj;
                }
            })

            if(givenBatsManStrike[year.season] !== undefined){
                givenBatsManStrike[year.season][0] += parseInt(delObj.total_runs);
                givenBatsManStrike[year.season][1] += parseInt(delObj.ball);
                // console.log(givenBatsManStrike);
            }
            else
            {
                givenBatsManStrike[year.season] = [];
                givenBatsManStrike[year.season].push(parseInt(delObj.total_runs),parseInt(delObj.ball));
                // console.log(givenBatsManStrike);
            }
        }
    })
    
    // console.log(givenBatsManStrike);
    // let finalObject;
    // finalObject = ((givenBatsManStrike[year][0]) / (givenBatsManStrike[year][1]) * 100);
    // console.log(finalObject);
    let myArr = [];
    for(let key in givenBatsManStrike){
        let value = ((givenBatsManStrike[key][0]) / (givenBatsManStrike[key][1]) * 100);
        myArr.push([key,value]);
    }

    // console.log(myArr);
    // convert the array into object
    let result = Object.fromEntries(myArr);
    console.log(result);
    return result;
}

// ###################################################################################################
// Find the highest number of times one player has been dismissed by another player
// dismissal type = [];
// traverse delivery obj iterate over each obj using filter check this object contain any
// dismissal type
// filtered all object in which dismissal type is contain
// for each on filtered object and extract bowler batsman and result
// if newobject not contain property add it and if already exist update
// convert object to enteries (enteries convert into array so we can easily sort array)

function highestNoOfTimesDismissedPlayer(deliveries){
    let newObj = {};
    deliveries.forEach(delObj => {
        if(delObj.dismissal_kind !== undefined){
            let batsmanName = delObj.batsman;
            let bowlerName = delObj.bowler;
            let result = batsmanName + "_" + bowlerName;
            if(!newObj[result]){
                 newObj[result] = 1;
            }
            else
            {
                newObj[result] += 1;
            }

        }
    })
    // convert obj to array
    let convertObjtoarray = Object.entries(newObj);
    // console.log(convertObjtoarray);
    convertObjtoarray.sort(function (a,b){
        return b[1] - a[1];
    });


    // console.log(convertObjtoarray);
    // console.log(convertObjtoarray[0]);
    let value = convertObjtoarray[0];
    console.log(value);
      
}


// Find the bowler with the best economy in super overs  (super over value is 0 so it give answer undefined I have taken 1 as value)

function bestEconomyInSuperOvers(deliveries){
    const myObj = {};
    deliveries.forEach(delObj => {
        if(myObj[delObj.bowler] !== undefined){
             myObj[delObj.bowler][0] += parseInt(delObj.total_runs);
            myObj[delObj.bowler][1] += 1;
        }
        else
        {
            myObj[delObj.bowler] = [];
            myObj[delObj.bowler].push(parseInt(delObj.total_runs),1);
        }
    })

    // console.log(myObj);
    let Arr = [];
    for(let key in myObj){
        let value = (myObj[key][0])/ (myObj[key][1]);
        Arr.push([key,value]);
    }

    // console.log(Arr);
    Arr.sort(function(a,b){
        return a[1] - b[1];
    });

    let bestEconomy = Arr[0][0];
    console.log(bestEconomy);
}



module.exports = {
    getMatchesPerYear,
    noOfMatchesWonPerYear,
    extraRuns2016,
    topEconomialBowler,
    noOfTimesWonTossAndMatch,
    highestPlayerOfMatch,
    strikeRateOfBatsman,
    highestNoOfTimesDismissedPlayer,
    bestEconomyInSuperOvers
    


    


};